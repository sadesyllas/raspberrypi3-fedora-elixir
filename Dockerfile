FROM resin/raspberrypi3-fedora

ENV LANG=en_US.UTF-8

WORKDIR /home

RUN \
  dnf -y install \
    sudo \
    bluez \
    wget \
    nmap \
    git \
    autoconf \
    make \
    gcc \
    gcc-c++ \
    ncurses-devel \
    wxGTK-devel \
    fop \
    openssl-devel \
    libxslt \
    unixODBC-devel \
    java-1.8.0-openjdk-devel \
    redhat-rpm-config && \
  dnf clean all

RUN \
  git clone https://github.com/erlang/otp && \
  cd otp && \
  git checkout OTP-20.3.8 && \
  ./otp_build setup && \
  make && \
  make install && \
  cd .. && \
  git clone https://github.com/elixir-lang/elixir && \
  cd elixir && \
  git checkout v1.6.5 && \
  make && \
  make install && \
  cd .. && \
  rm -rf otp elixir
